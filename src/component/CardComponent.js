import React, { useEffect, useState } from "react";
import { styled } from '@mui/material/styles';
import Card from '@mui/material/Card';
import CardMedia from '@mui/material/CardMedia';
import CardContent from '@mui/material/CardContent';
import CardActions from '@mui/material/CardActions';
import Collapse from '@mui/material/Collapse';
import IconButton from '@mui/material/IconButton';
import Typography from '@mui/material/Typography';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import userIcon from '../assets/avatar2.png'

const ExpandMore = styled((props) => {
  const { expand, ...other } = props;
  return <IconButton {...other} />;
})(({ theme, expand }) => ({
  transform: !expand ? 'rotate(0deg)' : 'rotate(180deg)',
  marginLeft: 'auto',
  transition: theme.transitions.create('transform', {
    duration: theme.transitions.duration.shortest,
  }),
}));

const useStyles = styled((theme) => ({
    imgS: {
        flex: 1,
        width: 50,
        height: 100,
        resizeMode: 'contain' 
    },
  }));


export default function RecipeReviewCard(props) {
  const [expanded, setExpanded] = React.useState(false);

  const handleExpandClick = () => {
    setExpanded(!expanded);
  };

  const styleClass = useStyles();

  

  return (
    <Card sx={{ maxWidth: 600 }}>
      <CardMedia
        component="img"
        height="194"
        width="400"
        image={userIcon}
        alt="Paella dish"
        className={styleClass.imgS}
      />
      <CardActions disableSpacing>
        <div>
            <p> User {props.user}</p>
        </div>
        
        <ExpandMore
          expand={expanded}
          onClick={handleExpandClick}
          aria-expanded={expanded}
          aria-label="show more"
        >
          <ExpandMoreIcon />
        </ExpandMore>
      </CardActions>
      <Collapse in={expanded} timeout="auto" unmountOnExit>
        <CardContent>
          <Typography paragraph>User ID :</Typography>
            <Typography paragraph>
              <h2>{props.user_id}</h2>
            </Typography>
            <Typography paragraph>Title :</Typography>
              <Typography paragraph>
                <h2>{props.title}</h2>
              </Typography>
            <Typography paragraph>Body :</Typography>
              <Typography paragraph>
                <h2>{props.body}</h2>
              </Typography>
            </CardContent>
    </Collapse>
    </Card>
  );
}