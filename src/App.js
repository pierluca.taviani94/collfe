import { BrowserRouter, Switch, Route } from "react-router-dom";
import FrontComp from "./pages/FrontComp";
import './App.css';


function App() {
  return (
    <BrowserRouter>
      <Switch>
        <Route path="/" exact component={FrontComp} />
      </Switch>
    </BrowserRouter>
  );
}

export default App;
