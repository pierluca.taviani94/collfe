import React, { useEffect, useState } from "react";
import { styled } from '@mui/material/styles';
import CardComponent from "../component/CardComponent";
import Grid from '@material-ui/core/Grid';
import acccessControl from '../service/url'


const useStyles = styled((theme) => ({
    root: {
        display: "flex",
        margin: 0,
        justifyContent: "center",
        alignItems: "center"
    },
    imgS: {
        flex: 1,
        width: 50,
        height: 100,
        resizeMode: 'contain' 
    },
}));


function FrontComp() {

    const styleClass = useStyles();

    const [data, setData] = useState(null);
    
    const url = "http://localhost:8080/my-api/posts";

    useEffect(() => {
        fetchData();

        async function fetchData() {

            var myHeaders = new Headers();
            myHeaders.append("Access-Control-Allow-Origin", acccessControl);
            myHeaders.append("Access-Control-Allow-Credentials", "true");

            var requestOptions = {
            method: "GET",
            headers: myHeaders,
            redirect: "follow",
            };

            const response = await fetch(url, requestOptions);
        
            const dataAcq = await response.json();

            setData(dataAcq);
        }

    }, []);

    //console.log(data);

    return(
        
        <Grid
            container
            spacing={0}
            direction="column"
            alignItems="center"
            justify="center"
            style={{ minHeight: '200vh' }}
            >
            <Grid item xs={6}>
                <div>
                    <h1> TEST POST </h1>
                </div>
            </Grid>  
            <Grid item xs={6}>
                <div className={styleClass.root}>
                    {/*<CardComponent />*/}
                </div>
            </Grid>      
            <Grid>

                 {/* display post from the API */}
                    {data && (
                    <div className="">

                        {/* loop over the posts */}
                        {data.map((posts, index) => (
                        <div key={index}>
                            <h2></h2>
                            <CardComponent user={posts.id} user_id={posts.user_id} title={posts.title} body= {posts.body}/>
                        </div>
                        ))}

                    </div>
                    
                    )}
            </Grid>
        </Grid>
        
    );
}

export default FrontComp;